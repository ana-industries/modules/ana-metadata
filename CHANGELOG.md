## 0.0.5

- `FilterMetaData` will carry a `inMandatory` flag if it is mandatory to include in find's filter.

## 0.0.4

- Filter to hold a 'parent', indicate whose is the parent of this field, e.g. 'props'
- Added `IsGroupParam` to Schema's rule, to indicate a field will be used for grouping up similar data.
- Removed `order` from Schema's rule.

## 0.0.3

- Schema now use a list of Rules. Removed mapping. Simple data, less errors.

## 0.0.2

- Add conflicts

## 0.0.0

- Initial version, created by Stagehand
