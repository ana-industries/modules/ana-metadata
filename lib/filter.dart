//TODO: Doc and fix naming if necessary
enum FilterType { selection, number, toggle }

class Filter {
  final String id;
  final String parent;
  final FilterType type;
  final dynamic value;

  get selector => parent != null ? '$parent.$id' : id;

  String toString() => 'filter id: $selector\nfilter value: $value';

  Filter(this.id, this.type, this.value, {this.parent});
}

class FilterMetaData {
  final String id;
  final FilterType type;
  final dynamic options;
  final bool isMandatory;

  String toString() => 'filter metadata id: $id\ntype: $type\noptions:$options';

  FilterMetaData(this.id, this.type, this.options, {this.isMandatory = false});
}
