// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'metadata.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const MetaDataType _$schema = const MetaDataType._('schema');
const MetaDataType _$commonWord = const MetaDataType._('commonWord');

MetaDataType _$metaDataTypeValueOf(String name) {
  switch (name) {
    case 'schema':
      return _$schema;
    case 'commonWord':
      return _$commonWord;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<MetaDataType> _$metaDataTypeValues =
    new BuiltSet<MetaDataType>(const <MetaDataType>[
  _$schema,
  _$commonWord,
]);

const RuleType _$text = const RuleType._('text');
const RuleType _$number = const RuleType._('number');
const RuleType _$toggle = const RuleType._('toggle');
const RuleType _$ref = const RuleType._('ref');
const RuleType _$dropdown = const RuleType._('dropdown');

RuleType _$ruleTypeValueOf(String name) {
  switch (name) {
    case 'text':
      return _$text;
    case 'number':
      return _$number;
    case 'toggle':
      return _$toggle;
    case 'ref':
      return _$ref;
    case 'dropdown':
      return _$dropdown;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<RuleType> _$ruleTypeValues =
    new BuiltSet<RuleType>(const <RuleType>[
  _$text,
  _$number,
  _$toggle,
  _$ref,
  _$dropdown,
]);

Serializer<MetaDataType> _$metaDataTypeSerializer =
    new _$MetaDataTypeSerializer();
Serializer<RuleType> _$ruleTypeSerializer = new _$RuleTypeSerializer();
Serializer<Rule> _$ruleSerializer = new _$RuleSerializer();
Serializer<Schema> _$schemaSerializer = new _$SchemaSerializer();
Serializer<CommonWord> _$commonWordSerializer = new _$CommonWordSerializer();

class _$MetaDataTypeSerializer implements PrimitiveSerializer<MetaDataType> {
  @override
  final Iterable<Type> types = const <Type>[MetaDataType];
  @override
  final String wireName = 'MetaDataType';

  @override
  Object serialize(Serializers serializers, MetaDataType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  MetaDataType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      MetaDataType.valueOf(serialized as String);
}

class _$RuleTypeSerializer implements PrimitiveSerializer<RuleType> {
  @override
  final Iterable<Type> types = const <Type>[RuleType];
  @override
  final String wireName = 'RuleType';

  @override
  Object serialize(Serializers serializers, RuleType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  RuleType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RuleType.valueOf(serialized as String);
}

class _$RuleSerializer implements StructuredSerializer<Rule> {
  @override
  final Iterable<Type> types = const [Rule, _$Rule];
  @override
  final String wireName = 'Rule';

  @override
  Iterable<Object> serialize(Serializers serializers, Rule object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ruleType',
      serializers.serialize(object.ruleType,
          specifiedType: const FullType(RuleType)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];
    if (object.characterLimit != null) {
      result
        ..add('characterLimit')
        ..add(serializers.serialize(object.characterLimit,
            specifiedType: const FullType(int)));
    }
    if (object.isMandatory != null) {
      result
        ..add('isMandatory')
        ..add(serializers.serialize(object.isMandatory,
            specifiedType: const FullType(bool)));
    }
    if (object.isFilter != null) {
      result
        ..add('isFilter')
        ..add(serializers.serialize(object.isFilter,
            specifiedType: const FullType(bool)));
    }
    if (object.isGroupParam != null) {
      result
        ..add('isGroupParam')
        ..add(serializers.serialize(object.isGroupParam,
            specifiedType: const FullType(bool)));
    }
    if (object.isInheritable != null) {
      result
        ..add('isInheritable')
        ..add(serializers.serialize(object.isInheritable,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  Rule deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RuleBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ruleType':
          result.ruleType = serializers.deserialize(value,
              specifiedType: const FullType(RuleType)) as RuleType;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'characterLimit':
          result.characterLimit = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'isMandatory':
          result.isMandatory = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isFilter':
          result.isFilter = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isGroupParam':
          result.isGroupParam = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'isInheritable':
          result.isInheritable = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$SchemaSerializer implements StructuredSerializer<Schema> {
  @override
  final Iterable<Type> types = const [Schema, _$Schema];
  @override
  final String wireName = 'Schema';

  @override
  Iterable<Object> serialize(Serializers serializers, Schema object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'rules',
      serializers.serialize(object.rules,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Rule)])),
      '_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.extra != null) {
      result
        ..add('extra')
        ..add(serializers.serialize(object.extra,
            specifiedType: const FullType(BuiltMap,
                const [const FullType(String), const FullType(String)])));
    }
    if (object.rev != null) {
      result
        ..add('_rev')
        ..add(serializers.serialize(object.rev,
            specifiedType: const FullType(String)));
    }
    if (object.deleted != null) {
      result
        ..add('_deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.conflicts != null) {
      result
        ..add('_conflicts')
        ..add(serializers.serialize(object.conflicts,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  Schema deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SchemaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'rules':
          result.rules.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Rule)]))
              as BuiltList<dynamic>);
          break;
        case 'extra':
          result.extra.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(String),
                const FullType(String)
              ])) as BuiltMap<dynamic, dynamic>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_rev':
          result.rev = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case '_conflicts':
          result.conflicts.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$CommonWordSerializer implements StructuredSerializer<CommonWord> {
  @override
  final Iterable<Type> types = const [CommonWord, _$CommonWord];
  @override
  final String wireName = 'CommonWord';

  @override
  Iterable<Object> serialize(Serializers serializers, CommonWord object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'schema',
      serializers.serialize(object.schema,
          specifiedType: const FullType(String)),
      'rule',
      serializers.serialize(object.rule, specifiedType: const FullType(String)),
      'words',
      serializers.serialize(object.words,
          specifiedType:
              const FullType(BuiltList, const [const FullType(String)])),
      '_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.rev != null) {
      result
        ..add('_rev')
        ..add(serializers.serialize(object.rev,
            specifiedType: const FullType(String)));
    }
    if (object.deleted != null) {
      result
        ..add('_deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.conflicts != null) {
      result
        ..add('_conflicts')
        ..add(serializers.serialize(object.conflicts,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  CommonWord deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CommonWordBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'schema':
          result.schema = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'rule':
          result.rule = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'words':
          result.words.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_rev':
          result.rev = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case '_conflicts':
          result.conflicts.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

abstract class CouchyBuilder {
  void replace(Couchy other);
  void update(void Function(CouchyBuilder) updates);
  String get id;
  set id(String id);

  String get rev;
  set rev(String rev);

  bool get deleted;
  set deleted(bool deleted);

  ListBuilder<String> get conflicts;
  set conflicts(ListBuilder<String> conflicts);
}

abstract class PropsyBuilder implements CouchyBuilder {
  void replace(covariant Propsy other);
  void update(void Function(PropsyBuilder) updates);
  MapBuilder<String, JsonObject> get props;
  set props(MapBuilder<String, JsonObject> props);

  String get id;
  set id(String id);

  String get rev;
  set rev(String rev);

  bool get deleted;
  set deleted(bool deleted);

  ListBuilder<String> get conflicts;
  set conflicts(ListBuilder<String> conflicts);
}

class _$Rule extends Rule {
  @override
  final RuleType ruleType;
  @override
  final String name;
  @override
  final int characterLimit;
  @override
  final bool isMandatory;
  @override
  final bool isFilter;
  @override
  final bool isGroupParam;
  @override
  final bool isInheritable;

  factory _$Rule([void Function(RuleBuilder) updates]) =>
      (new RuleBuilder()..update(updates)).build();

  _$Rule._(
      {this.ruleType,
      this.name,
      this.characterLimit,
      this.isMandatory,
      this.isFilter,
      this.isGroupParam,
      this.isInheritable})
      : super._() {
    if (ruleType == null) {
      throw new BuiltValueNullFieldError('Rule', 'ruleType');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('Rule', 'name');
    }
  }

  @override
  Rule rebuild(void Function(RuleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RuleBuilder toBuilder() => new RuleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Rule &&
        ruleType == other.ruleType &&
        name == other.name &&
        characterLimit == other.characterLimit &&
        isMandatory == other.isMandatory &&
        isFilter == other.isFilter &&
        isGroupParam == other.isGroupParam &&
        isInheritable == other.isInheritable;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, ruleType.hashCode), name.hashCode),
                        characterLimit.hashCode),
                    isMandatory.hashCode),
                isFilter.hashCode),
            isGroupParam.hashCode),
        isInheritable.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Rule')
          ..add('ruleType', ruleType)
          ..add('name', name)
          ..add('characterLimit', characterLimit)
          ..add('isMandatory', isMandatory)
          ..add('isFilter', isFilter)
          ..add('isGroupParam', isGroupParam)
          ..add('isInheritable', isInheritable))
        .toString();
  }
}

class RuleBuilder implements Builder<Rule, RuleBuilder> {
  _$Rule _$v;

  RuleType _ruleType;
  RuleType get ruleType => _$this._ruleType;
  set ruleType(RuleType ruleType) => _$this._ruleType = ruleType;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _characterLimit;
  int get characterLimit => _$this._characterLimit;
  set characterLimit(int characterLimit) =>
      _$this._characterLimit = characterLimit;

  bool _isMandatory;
  bool get isMandatory => _$this._isMandatory;
  set isMandatory(bool isMandatory) => _$this._isMandatory = isMandatory;

  bool _isFilter;
  bool get isFilter => _$this._isFilter;
  set isFilter(bool isFilter) => _$this._isFilter = isFilter;

  bool _isGroupParam;
  bool get isGroupParam => _$this._isGroupParam;
  set isGroupParam(bool isGroupParam) => _$this._isGroupParam = isGroupParam;

  bool _isInheritable;
  bool get isInheritable => _$this._isInheritable;
  set isInheritable(bool isInheritable) =>
      _$this._isInheritable = isInheritable;

  RuleBuilder();

  RuleBuilder get _$this {
    if (_$v != null) {
      _ruleType = _$v.ruleType;
      _name = _$v.name;
      _characterLimit = _$v.characterLimit;
      _isMandatory = _$v.isMandatory;
      _isFilter = _$v.isFilter;
      _isGroupParam = _$v.isGroupParam;
      _isInheritable = _$v.isInheritable;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Rule other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Rule;
  }

  @override
  void update(void Function(RuleBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Rule build() {
    final _$result = _$v ??
        new _$Rule._(
            ruleType: ruleType,
            name: name,
            characterLimit: characterLimit,
            isMandatory: isMandatory,
            isFilter: isFilter,
            isGroupParam: isGroupParam,
            isInheritable: isInheritable);
    replace(_$result);
    return _$result;
  }
}

class _$Schema extends Schema {
  @override
  final BuiltList<Rule> rules;
  @override
  final BuiltMap<String, String> extra;
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;
  @override
  final BuiltList<String> conflicts;

  factory _$Schema([void Function(SchemaBuilder) updates]) =>
      (new SchemaBuilder()..update(updates)).build();

  _$Schema._(
      {this.rules, this.extra, this.id, this.rev, this.deleted, this.conflicts})
      : super._() {
    if (rules == null) {
      throw new BuiltValueNullFieldError('Schema', 'rules');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('Schema', 'id');
    }
  }

  @override
  Schema rebuild(void Function(SchemaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SchemaBuilder toBuilder() => new SchemaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Schema &&
        rules == other.rules &&
        extra == other.extra &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted &&
        conflicts == other.conflicts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, rules.hashCode), extra.hashCode), id.hashCode),
                rev.hashCode),
            deleted.hashCode),
        conflicts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Schema')
          ..add('rules', rules)
          ..add('extra', extra)
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted)
          ..add('conflicts', conflicts))
        .toString();
  }
}

class SchemaBuilder implements Builder<Schema, SchemaBuilder>, CouchyBuilder {
  _$Schema _$v;

  ListBuilder<Rule> _rules;
  ListBuilder<Rule> get rules => _$this._rules ??= new ListBuilder<Rule>();
  set rules(ListBuilder<Rule> rules) => _$this._rules = rules;

  MapBuilder<String, String> _extra;
  MapBuilder<String, String> get extra =>
      _$this._extra ??= new MapBuilder<String, String>();
  set extra(MapBuilder<String, String> extra) => _$this._extra = extra;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  ListBuilder<String> _conflicts;
  ListBuilder<String> get conflicts =>
      _$this._conflicts ??= new ListBuilder<String>();
  set conflicts(ListBuilder<String> conflicts) => _$this._conflicts = conflicts;

  SchemaBuilder();

  SchemaBuilder get _$this {
    if (_$v != null) {
      _rules = _$v.rules?.toBuilder();
      _extra = _$v.extra?.toBuilder();
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _conflicts = _$v.conflicts?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant Schema other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Schema;
  }

  @override
  void update(void Function(SchemaBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Schema build() {
    _$Schema _$result;
    try {
      _$result = _$v ??
          new _$Schema._(
              rules: rules.build(),
              extra: _extra?.build(),
              id: id,
              rev: rev,
              deleted: deleted,
              conflicts: _conflicts?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'rules';
        rules.build();
        _$failedField = 'extra';
        _extra?.build();

        _$failedField = 'conflicts';
        _conflicts?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Schema', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$CommonWord extends CommonWord {
  @override
  final String schema;
  @override
  final String rule;
  @override
  final BuiltList<String> words;
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;
  @override
  final BuiltList<String> conflicts;

  factory _$CommonWord([void Function(CommonWordBuilder) updates]) =>
      (new CommonWordBuilder()..update(updates)).build();

  _$CommonWord._(
      {this.schema,
      this.rule,
      this.words,
      this.id,
      this.rev,
      this.deleted,
      this.conflicts})
      : super._() {
    if (schema == null) {
      throw new BuiltValueNullFieldError('CommonWord', 'schema');
    }
    if (rule == null) {
      throw new BuiltValueNullFieldError('CommonWord', 'rule');
    }
    if (words == null) {
      throw new BuiltValueNullFieldError('CommonWord', 'words');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('CommonWord', 'id');
    }
  }

  @override
  CommonWord rebuild(void Function(CommonWordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CommonWordBuilder toBuilder() => new CommonWordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CommonWord &&
        schema == other.schema &&
        rule == other.rule &&
        words == other.words &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted &&
        conflicts == other.conflicts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, schema.hashCode), rule.hashCode),
                        words.hashCode),
                    id.hashCode),
                rev.hashCode),
            deleted.hashCode),
        conflicts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CommonWord')
          ..add('schema', schema)
          ..add('rule', rule)
          ..add('words', words)
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted)
          ..add('conflicts', conflicts))
        .toString();
  }
}

class CommonWordBuilder
    implements Builder<CommonWord, CommonWordBuilder>, CouchyBuilder {
  _$CommonWord _$v;

  String _schema;
  String get schema => _$this._schema;
  set schema(String schema) => _$this._schema = schema;

  String _rule;
  String get rule => _$this._rule;
  set rule(String rule) => _$this._rule = rule;

  ListBuilder<String> _words;
  ListBuilder<String> get words => _$this._words ??= new ListBuilder<String>();
  set words(ListBuilder<String> words) => _$this._words = words;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  ListBuilder<String> _conflicts;
  ListBuilder<String> get conflicts =>
      _$this._conflicts ??= new ListBuilder<String>();
  set conflicts(ListBuilder<String> conflicts) => _$this._conflicts = conflicts;

  CommonWordBuilder();

  CommonWordBuilder get _$this {
    if (_$v != null) {
      _schema = _$v.schema;
      _rule = _$v.rule;
      _words = _$v.words?.toBuilder();
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _conflicts = _$v.conflicts?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant CommonWord other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CommonWord;
  }

  @override
  void update(void Function(CommonWordBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CommonWord build() {
    _$CommonWord _$result;
    try {
      _$result = _$v ??
          new _$CommonWord._(
              schema: schema,
              rule: rule,
              words: words.build(),
              id: id,
              rev: rev,
              deleted: deleted,
              conflicts: _conflicts?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'words';
        words.build();

        _$failedField = 'conflicts';
        _conflicts?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CommonWord', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
