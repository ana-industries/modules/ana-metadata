import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';

// Exporting these so that the user of `Couchy` and `Propsy` do not
// get a surprise compilation error from missing import in generated file.
export 'package:built_collection/built_collection.dart';
export 'package:built_value/json_object.dart';
export 'package:built_value/built_value.dart';

part 'metadata.g.dart';

/// Couchy supplements built_value classes with CouchDB semantics.
///
/// ```dart
/// abstract class CouchDBDoc implements Couchy, Built<CouchDBDoc, CouchDBDocBuilder> {
///
///   // getter goes here
///
///   factory CouchDBDoc([updates(CouchDBDocBuilder b)]) = _$CouchDBDoc;
///   CouchDBDoc._();
/// }
/// ```
///
/// Now, we can do CouchDB stuff with the instance of that class
///
/// ```dart
/// var doc = CouchDBDoc((builder) => builder..id = "4421");
///
/// // Upload document and update local's document revision
/// var newRevision = _uploadDocumentToDB(doc);
/// var updatedDoc = doc.rebuild((builder) => builder..rev = newRevision);
///
/// // Mark a document as deleted
/// var deletedDoc = updateDoc.rebuild((builder) => builder..deleted = true);
/// _uploadDocumentToDB(deletedDoc);
/// ```
///
@BuiltValue(instantiable: false)
abstract class Couchy extends Object {
  /// `id` need to translate to `_id` for serialized object.
  /// That's how CouchDB recognise this document. But `_` make funny situation in Dart.
  /// Same for `rev` and `deleted`.
  @BuiltValueField(wireName: "_id")
  String get id;

  @nullable
  @BuiltValueField(wireName: "_rev")
  String get rev;

  @nullable
  @BuiltValueField(wireName: "_deleted")
  bool get deleted;

  @nullable
  @BuiltValueField(wireName: "_conflicts")
  BuiltList<String> get conflicts;
}

/// Propsy is the dynamic part of each type of ANA's classes
///
/// It contains `props`, a map of String to any type of data can be serialize into a JSON.
///
/// This is where documents in ANA store their application's specific content.
/// `Schema` provide a soft layout for the available fields and rules of each field.
///
/// ```dart
/// abstract class PropDoc implements Propsy, Built<PropDoc, PropDocBuilder> {
///
///   // getter goes here
///
///   factory PropDoc([updates(PropDocBuilder b)]) = _$PropDoc;
///   PropDoc._();
/// }
///
/// var doc = PropDoc.rebuild((builder) => builder
///   ..id = 'cool id'
///   ..props['Name'] = JsonObject('cool name')
///   ..props['Age'] = JsonObject(100));
///
/// ```
@BuiltValue(instantiable: false)
abstract class Propsy extends Object implements Couchy {
  @nullable
  BuiltMap<String, JsonObject> get props;
}

/// MetaDataType declare available types of metadata
///
/// `schema` give a soft data layout for each class implemented `Propsy`.
/// `commonWord` provide a list of suggestions text/options value for each rule in `Schema`.
///
class MetaDataType extends EnumClass {
  static const MetaDataType schema = _$schema;
  static const MetaDataType commonWord = _$commonWord;

  static BuiltSet<MetaDataType> get values => _$metaDataTypeValues;
  static MetaDataType valueOf(String name) => _$metaDataTypeValueOf(name);

  static Serializer<MetaDataType> get serializer => _$metaDataTypeSerializer;
  const MetaDataType._(String name) : super(name);
}

/// RuleType declare available types of rule to restrict/define each data field.
///
/// `text` is all characters text field.
/// `number` is.. for number?
/// `toggle` is Yes/No, On/Off, Active/Passive, etc.
/// `ref` is referencing to another `Schema`'s `Rule`.
///   This is useful for class that derive data from another class.
/// `dropdown` is for dropdown menus
///
class RuleType extends EnumClass {
  static const RuleType text = _$text;
  static const RuleType number = _$number;
  static const RuleType toggle = _$toggle;
  static const RuleType ref = _$ref;
  static const RuleType dropdown = _$dropdown;

  static BuiltSet<RuleType> get values => _$ruleTypeValues;
  static RuleType valueOf(String name) => _$ruleTypeValueOf(name);

  static Serializer<RuleType> get serializer => _$ruleTypeSerializer;
  const RuleType._(String name) : super(name);
}

/// Rule is the metadata for each field in a `Schema`.
///
/// `characterLimit` not that JSON or Dart has a limit on character, this is more for the export target's limitation
/// `isMandatory` a flag useful for form's validation
/// `isFilter` a flag useful to generate filter options, both in CouchDB and Application
/// `isGroupParam` a flag to define this field can be used to group similar data
/// `isInheritable` a flag to signal this field can be copy/derive to other class with other `Schema`
///
abstract class Rule implements Built<Rule, RuleBuilder> {
  RuleType get ruleType;

  String get name;
  @nullable
  int get characterLimit;
  @nullable
  bool get isMandatory;
  @nullable
  bool get isFilter;
  @nullable
  bool get isGroupParam;
  @nullable
  bool get isInheritable;

  static Serializer<Rule> get serializer => _$ruleSerializer;
  factory Rule(RuleType ruleType, String name, {int characterLimit}) =>
      _$Rule._(ruleType: ruleType, name: name, characterLimit: characterLimit);
  Rule._();
}

/// Schema define a soft data layout for a document in ANA.
///
/// Schema itself is a document store inside the database.
/// It will be fetched out and passed around ANA in helping
/// - generating forms' label and field to fill up one instance of document
/// - generating table and it's header
/// - generating Filter options
///
/// Document in ANA is not necessary follow strictly to `Schema`, thus it is just a soft layout.
///
/// `rules` is a list of `Rule`
/// `extra` is non-rule information to store in this `Schema`. e.g. The key to a very specific field.
///
abstract class Schema implements Couchy, Built<Schema, SchemaBuilder> {
  BuiltList<Rule> get rules;
  @nullable
  BuiltMap<String, String> get extra;

  static Serializer<Schema> get serializer => _$schemaSerializer;
  factory Schema([updates(SchemaBuilder b)]) = _$Schema;
  Schema._();
}

/// CommonWord store a list of suggestion text for each field in the schema.
///
/// `schema` is the name of the schema
/// `rule` is the name of the rule
/// These 2 combo make up which field in a data the list of text belongs to.
///
abstract class CommonWord
    implements Couchy, Built<CommonWord, CommonWordBuilder> {
  String get schema;
  String get rule;
  BuiltList<String> get words;

  static Serializer<CommonWord> get serializer => _$commonWordSerializer;
  factory CommonWord([updates(CommonWordBuilder b)]) = _$CommonWord;
  CommonWord._();
}
