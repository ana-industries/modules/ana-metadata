import 'package:built_value/json_object.dart';
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';

import 'test_data.dart';

/// We really have nothing functionality worth a test in this package.
/// The test here is simply to get a consent from the editor for the changes.
/// Also it will failed a rogue commit, (if our CI is setup correctly).
void main() {
  group('guard againts changing any class members', () {
    test('couchy and propsy', () {
      final couchy = CouchDBDoc((builder) => builder
        ..id = 'test id'
        ..rev = 'test rev'
        ..deleted = false
        ..conflicts.add('test conflict'));

      expect(couchy, isNotNull);

      final propsy = PropDoc((builder) => builder
        ..id = 'test id 2'
        ..rev = 'test rev 2'
        ..deleted = false
        ..props['test key'] = JsonObject('test value'));

      expect(propsy, isNotNull);
    });

    test('enums', () {
      expect(MetaDataType.values.length, 2);
      expect(MetaDataType.schema, isNotNull);
      expect(MetaDataType.commonWord, isNotNull);

      expect(RuleType.values.length, 5);
      expect(RuleType.text, isNotNull);
      expect(RuleType.number, isNotNull);
      expect(RuleType.toggle, isNotNull);
      expect(RuleType.ref, isNotNull);
      expect(RuleType.dropdown, isNotNull);
    });

    test('Schema with its rule', () {
      final rule = (RuleBuilder()
            ..ruleType = RuleType.text
            ..name = 'testing rule'
            ..characterLimit = 20
            ..isMandatory = false
            ..isFilter = true
            ..isGroupParam = false
            ..isInheritable = false)
          .build();

      final schema = Schema((builder) => builder
        ..id = 'id 1'
        ..rev = 'rev 1'
        ..deleted = false
        ..rules.add(rule)
        ..extra['b'] = 'c');

      expect(schema, isNotNull);
    });

    test('CommonWord', () {
      final word = CommonWord((builder) => builder
        ..id = 'id'
        ..rev = 'rev'
        ..deleted = true
        ..schema = 's'
        ..rule = 'r'
        ..words = ListBuilder(['a', 'b', 'c']));

      expect(word, isNotNull);
    });
  });
}
