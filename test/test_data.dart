import 'package:ana_metadata/metadata.dart';

part 'test_data.g.dart';

abstract class CouchDBDoc
    implements Couchy, Built<CouchDBDoc, CouchDBDocBuilder> {
  factory CouchDBDoc([updates(CouchDBDocBuilder b)]) = _$CouchDBDoc;
  CouchDBDoc._();
}

abstract class PropDoc implements Propsy, Built<PropDoc, PropDocBuilder> {
  // getter goes here
  factory PropDoc([updates(PropDocBuilder b)]) = _$PropDoc;
  PropDoc._();
}
