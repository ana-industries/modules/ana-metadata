# ANA internal module for metadata

All documents to be store into the database has to implement `Couchy` or `Propsy`.

`Schema` is there to give a soft data layout for each type of document. It contains `Rule` to define the available field in a type of document and its usage.

`CommonWord` tie a list of text for each `Rule` in the `Schema`. Think of it like input suggestion or drop-down options.

All classes in this package and ANA made strong use of [Built Value](https://github.com/google/built_value.dart). You need to understand this fully to be able to understand how to manipulate data in ANA.

## Note on usage

Usually you do not want to make changes to these files, unless ANA is making fundamental changes to it's data. Any changes in this file will have a cascading effect. It can cause data corruption or missing data if not done carefully.

Any changes in this package must run a `pub run build_runner build` or `pub run build_runner watch` at the end, and commit the generated file to the git repo. This is so that all packages and ANA, who depends on this package, will function correctly.

## Development

### Git branches

`master` is protected and reserve for main application development.

All active development should happen on `dev` branch. You should always branch out from `dev` and create push request against the `dev`.

### Pub Packages and ANA Modules

We open sourced a few building blocks of ANA, and these projects are ANA Modules. The cost of this is a tedious packages management in the main application.

Developers went through the trial projects will not be foreign to ANA Modules. Just a rule of thumb, we should always use the git packages unless you are working on feature related to that module.

### Style guide

[Dart's official guide](https://dart.dev/guides/language/effective-dart)

### Code Formatting

For `.dart`, use `dartfmt`. It is a tool came with Dart SDK. Do this in your command line `dartfmt -w .`

For `.html` `.scss` `.yaml`, use [prettier](https://prettier.io/docs/en/index.html). Install it and type `prettier --write "**/*.html" "**/*.scss" "**/*.yaml"` in your command line.

Better, if your IDE support it, just install the extension of these and enabled 'format on save'. It will be easier.

### Rules

- Commit and push your branch to GitLab, often.
- Have fun.
